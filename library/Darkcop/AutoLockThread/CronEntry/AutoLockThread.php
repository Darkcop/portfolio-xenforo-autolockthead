<?php

class Darkcop_AutoLockThread_CronEntry_AutoLockThread{
	public static function runAutoLockThread(){
		$days = XenForo_Application::get('options')->autoLockDays;
		$forums = XenForo_Application::get('options')->autoLockForums;
		$forumsAll = XenForo_Application::get('options')->autoLockForumsAll;
		$prefixId = XenForo_Application::get('options')->autoLockPrefix;

		if ($forums != ''){
			$timestamp = time() - (86400 * $days);
			$forums = rtrim($forums, ',');			
			$forumsArray = explode(',', $forums); 
			$whereclause = 'AND (xf_thread.node_id = ' . implode(' OR xf_thread.node_id = ', $forumsArray);
			$whereclause = $whereclause . ')';
			$db = XenForo_Application::get('db');
			$threads = $db->fetchAll("
			SELECT xf_thread.thread_id
			FROM xf_thread
			INNER JOIN xf_node ON xf_node.node_id = xf_thread.node_id
			WHERE xf_thread.last_post_date < " . $timestamp . "
			AND xf_thread.sticky = 0
			AND discussion_open = 1
			$whereclause
			");										
			
			foreach ($threads as $thread){
				$db->query("UPDATE xf_thread SET discussion_open = 0, prefix_id = " . $prefixId . " WHERE thread_id = ?", $thread['thread_id']);
			}
		}
		
		if ($forumsAll){	
			$timestamp = time() - (86400 * $days);
			$db = XenForo_Application::get('db');		
			
			$threads = $db->fetchAll("
			SELECT xf_thread.thread_id
			FROM xf_thread
			INNER JOIN xf_node ON xf_node.node_id = xf_thread.node_id
			WHERE xf_thread.last_post_date < " . $timestamp . "
			AND xf_thread.sticky = 0
			AND discussion_open = 1
			");

			foreach ($threads as $thread){
				$db->query("UPDATE xf_thread SET discussion_open = 0, prefix_id = " . $prefixId . " WHERE thread_id = ?", $thread['thread_id']);

				$theadid = ;$thread['thread_id']

				$db->query("INSERT INTO xf_post 
					(thread_id, user_id, username, post_date, message, ip_id, messsage_state, attack_count, position, likes, like_users, warning_id, warning_message, last_edit_date, last_edit_user_id, edit_count) 
					VALUES 
					(" . $theadid . ", ?,?,?)");
			}
		}
	}
}

?>